const axios = require('axios')

const api = require('marvel-api');

const marvel = api.createClient({
  publicKey: "13e200cfec78b510f96096a26507d508",
  privateKey: "89b0f49e9c48be300bc7f17b9c5dc2aaf0d8ad2c"
});



const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLSchema } = require('graphql')

const Characters = new GraphQLObjectType({
    name: 'Characters',
    fields: () =>({
        id: { type: GraphQLInt },
        name: { type: GraphQLString },
        description: { type: GraphQLString },
        thumbnail: {type: CharImgs}
        
    })
});

const CharImgs = new GraphQLObjectType({
    name: 'Thumbnail',
    fields: () =>({
        path: {type: GraphQLString},
        extension: {type: GraphQLString}
        
    })
});



// Root Query

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields:{
        Characters:{
            type: new GraphQLList(Characters),
            resolve(parent, args) {
                return marvel.characters.findAll()
                .then(res => res.data);
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery
})
