import React, { Component, Fragment } from 'react'
import gql from 'graphql-tag'
import {Query} from 'react-apollo'
import SingleCharacter from './SingleCharacter'

const Character_Query = gql`
query CharactersQuery{
    Characters {
        id
        name
        description
        thumbnail {
          path
          extension
        }
      }
}
`

export class Characters extends Component {
    render() {
        return(
            <div className='content-wrap'>
                <Query query={Character_Query}>
                {
                    ({ loading, error, data }) =>{
                        if (loading) return <h4>Loading...</h4>
                        if (error) console.log(error);
                        
                        return <Fragment>
                            {
                                data.Characters.map(character =>(
                                    <SingleCharacter key={Characters.id}  character={character}/>
                                ))
                            }
                        </Fragment>
                    }
                }
                </Query>
            </div>
        )
    }
}

export default Characters