import React from 'react'
import logo from '../images/logo.svg'
const NavBar = () => {
    return(
        <nav>
            <div className="nav-wrapper grey darken-4">
                <a href="#" className="brand-logo">
                    <img className="logo" src={logo} alt="Marvel Logo"/>
                </a>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li><a href="sass.html">Link</a></li>
                    <li><a href="badges.html">Link</a></li>
                    <li><a href="collapsible.html">Link</a></li>
                </ul>
            </div>
        </nav>
    )
}
export default NavBar;


