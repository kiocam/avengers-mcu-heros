import React from 'react'


export  default function SingleCharacter({ character: {id, name, description, thumbnail} }) {
  return (
    <div key={id} className="card medium">
    <div className="card-image waves-effect waves-block waves-light">
      <img className="activator" src={`${thumbnail.path}` + `.${thumbnail.extension}`} alt={name}/>
    </div>
    <div className="card-content">
      <span className="card-title activator grey-text text-darken-4">{name}<i className="material-icons">info</i></span>
      
    </div>
    <div className="card-reveal">
      <span className="card-title grey-text text-darken-4">{name}<i className="material-icons right">close</i></span>
      <p>{description}</p>
    </div>
  </div>
  )
}
