import React, { Component } from 'react'
import ApolloClient from 'apollo-boost'
import {ApolloProvider} from 'react-apollo'
import '../styles/App.scss'

import NavBar from '../components/navBar'
import Characters from '../components/Characters'

const client = new ApolloClient({
    uri: 'http://localhost:5000/graphql'
})



class App extends Component {
 render() {
     return(
         <ApolloProvider client={client}>
            <div className="App"> 
                <NavBar></NavBar>
                <Characters></Characters>          
            </div>
         </ApolloProvider>
     )
 }
}

export default App;
